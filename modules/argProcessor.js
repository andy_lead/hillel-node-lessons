const chalk = require('chalk');

module.exports = function(args) {
	let output = chalk.green('- - - - PROCESSED ARGUMENTS - - - -');
	
	for (var arg in args) {
		if (arg !== '_')
			output += '\n' + chalk.yellow(arg + ': ') + chalk.red(args[arg]);
	}

	return output;
};