const chalk = require('chalk');

exports.envProcessor = function(env, fields = 'all'){
	let output = chalk.green('- - - - ENV ARGUMENTS - - - -');

	for (var field in env) {
		if (fields === 'all' || fields.includes(field))
			output += '\n' + chalk.yellow(field + ': ') + chalk.red(env[field]);
	}

	return output;
};