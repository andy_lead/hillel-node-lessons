const chalk = require('chalk');

function osProcessor(os, fields = 'all') {
    let output = chalk.green('- - - - OS ARGUMENTS - - - -');
    
    for (field in os) {
    	if (fields === 'all' || fields.includes(field)) {
    		output += '\n' + chalk.yellow(field + ': ');
    		if (typeof os[field] === 'function')
    			output += chalk.red(JSON.stringify(os[field](), 1));
    		else if (typeof os[field] === 'object')
    			output += chalk.green(JSON.stringify(os[field], 1));
    		else
    			output += chalk.red(os[field])
    	}
    }

    return output;
}

global.osProcessor = osProcessor;