"use strict";
const chalk = require('chalk');
const argv = require('minimist')(process.argv.slice(2));
const os = require('os');

const argProcessor = require('./modules/argProcessor');
const { envProcessor } = require('./modules/envProcessor');
require('./modules/osProcessor');

console.log(argProcessor(argv));
console.log('\n');
console.log(envProcessor(process.env, ['NODE_ENV', 'USER', 'TERM_PROGRAM', 'PWD', 'npm_package_author_name']));
console.log('\n');
console.log(osProcessor(os, ['hostname', 'platform', 'userInfo', 'uptime']));